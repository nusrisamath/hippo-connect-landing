const Header = () => {
  return (
    <header className="fixed w-full opacity-90 z-90">
      <nav className="bg-white border-gray-200 py-2.5 dark:bg-gray-900">
        <div className="flex flex-wrap items-center justify-between max-w-screen-xl px-4 mx-auto">
          <a href="#" className="flex items-center">
            <button className="hidden md:block cursor-default">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="44"
                height="42"
                viewBox="0 0 44 42"
                fill="none"
              >
                <g>
                  <path
                    d="M37.2678 19.8887C36.0321 19.3239 34.7502 19.0011 33.4719 18.8971C33.4719 18.8971 33.4719 18.8962 33.4728 18.8953C32.1156 18.803 30.9677 17.936 30.5809 16.7291C30.1489 15.3807 30.863 14.2196 30.9694 14.0537C30.9677 14.0537 30.9659 14.0546 30.9632 14.0555C31.138 13.8251 31.2915 13.574 31.4157 13.2979C31.6809 12.7043 31.7847 12.0785 31.7474 11.4706C31.7474 11.4706 31.7483 11.4715 31.7492 11.4724C31.5922 9.4237 32.4624 7.51844 34.0237 6.57074C35.2417 5.83106 36.4943 5.90906 37.007 5.97182C37.007 5.97182 37.0052 5.97003 37.0052 5.96913C38.2383 6.09824 39.4687 5.43117 40.0081 4.22705C40.6823 2.72077 40.0205 0.94821 38.5302 0.266799C37.0398 -0.414611 35.2861 0.254247 34.6119 1.76052C34.4859 2.04116 34.4069 2.33165 34.3715 2.62125C34.3715 2.62215 34.3715 2.62394 34.3715 2.62574C34.3715 2.62663 34.3715 2.62843 34.3715 2.62932C34.3218 3.16279 34.1018 4.75694 32.8173 6.10721C30.9739 8.04474 28.4634 7.87439 28.1121 7.8448C28.113 7.8457 28.1148 7.84749 28.1157 7.84839C26.5961 7.77577 25.1164 8.62753 24.4511 10.1141C23.5808 12.0588 24.4351 14.3478 26.3592 15.2274C26.3858 15.239 26.4124 15.248 26.439 15.2587C26.4373 15.2587 26.4355 15.2596 26.4337 15.2605C27.4104 15.6658 28.0979 16.5624 28.2177 17.5988C28.3525 18.7644 27.7484 19.9174 26.6954 20.4921C26.6954 20.493 26.6954 20.4939 26.6963 20.4948C25.1661 21.4147 23.8363 22.7022 22.8507 24.3009C22.849 24.3009 22.8472 24.3 22.8445 24.2991C22.6698 24.5842 22.3593 24.7761 22.0027 24.7761C21.6461 24.7761 21.3427 24.5887 21.1661 24.3089C21.1644 24.3098 21.1635 24.3107 21.1617 24.3116C20.1752 22.7076 18.8428 21.4165 17.309 20.4948C17.309 20.4948 17.309 20.493 17.3099 20.4921C16.2561 19.9174 15.6528 18.7644 15.7877 17.5988C15.9074 16.5624 16.5949 15.6658 17.5716 15.2605C17.5698 15.2605 17.5681 15.2596 17.5663 15.2587C17.5929 15.2471 17.6195 15.239 17.6461 15.2274C19.5702 14.3478 20.4245 12.0588 19.5543 10.1141C18.889 8.62753 17.4093 7.77666 15.8897 7.84839C15.8906 7.84749 15.8923 7.8457 15.8932 7.8448C15.5428 7.87439 13.0323 8.04474 11.1881 6.10721C9.90355 4.75694 9.68355 3.16369 9.63388 2.62932C9.63388 2.62843 9.63388 2.62663 9.63388 2.62574C9.63388 2.62484 9.63388 2.62305 9.63388 2.62125C9.5975 2.33076 9.51855 2.04116 9.39347 1.76052C8.71573 0.255144 6.96105 -0.413715 5.47162 0.266799C3.98129 0.94821 3.31952 2.72077 3.99371 4.22705C4.53307 5.43207 5.76259 6.09824 6.99654 5.96913C6.99654 5.96913 6.99476 5.97092 6.99476 5.97182C7.50662 5.90906 8.76009 5.83106 9.97807 6.57074C11.5394 7.51844 12.4087 9.4237 12.2526 11.4724C12.2526 11.4724 12.2535 11.4715 12.2544 11.4706C12.2171 12.0785 12.32 12.7043 12.5861 13.2979C12.7094 13.574 12.8638 13.8251 13.0386 14.0555C13.0368 14.0555 13.035 14.0546 13.0323 14.0537C13.1379 14.2196 13.852 15.3807 13.4209 16.7291C13.0341 17.936 11.8862 18.803 10.529 18.8953V18.8971C9.25154 19.0011 7.96968 19.3239 6.73396 19.8887C0.972264 22.5211 -1.58524 29.3738 1.01928 35.1954C3.62379 41.0169 10.4048 43.6018 16.1638 40.9694C18.4569 39.9213 20.2329 38.1981 21.3861 36.1431C21.3888 36.1467 21.3923 36.1493 21.395 36.1529C21.5201 35.9476 21.7427 35.8086 21.9991 35.8086C22.2555 35.8086 22.4888 35.9539 22.6112 36.1682C22.6157 36.1664 22.6192 36.1646 22.6236 36.1628C23.7777 38.2097 25.5484 39.9249 27.8344 40.9694C33.5944 43.6018 40.3744 41.0161 42.979 35.1954C45.5835 29.3738 43.026 22.5211 37.2661 19.8887H37.2678Z"
                    fill="#66CCA1"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_757_3326">
                    <rect width="44" height="42" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </button>
            {/* <span className="self-center text-xl font-semibold whitespace-nowrap text-gray-900 dark:text-white">
              Hippo Connect
            </span> */}
          </a>
          <div className="flex items-center lg:order-2">
            {/* <!-- <a href="#" className="text-gray-800 dark:text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 lg:px-5 py-2 lg:py-2.5 sm:mr-2 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800">Log in</a> --> */}
            <a
              href="#contactUs"
              className="text-gray-900 dark:text-white border border-purple-700 hover:border-purple-800 focus:ring-4 focus:ring-purple-300 font-medium rounded-full text-sm px-4 lg:px-5 py-2 lg:py-2.5 sm:mr-2 lg:mr-0 bg-transparent focus:outline-none dark:focus:ring-purple-800"
            >
              Contact Us
            </a>
            <button
              data-collapse-toggle="mobile-menu-2"
              type="button"
              className="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg lg:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              aria-controls="mobile-menu-2"
              aria-expanded="false"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clip-rule="evenodd"
                ></path>
              </svg>
              <svg
                className="hidden w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </button>
          </div>
          <div
            className="items-center justify-between hidden w-full lg:flex lg:w-auto lg:order-1"
            id="mobile-menu-2"
          >
            <ul className="flex flex-col mt-4 font-medium lg:flex-row lg:space-x-8 lg:mt-0">
              <li>
                <a
                  href="#features"
                  className="block py-2 pl-3 pr-4 text-white bg-purple-700 rounded lg:bg-transparent lg:text-purple-700 lg:p-0 dark:text-white"
                  aria-current="page"
                >
                  Features
                </a>
              </li>
              <li>
                <a
                  href="#usecases"
                  className="block py-2 pl-3 pr-4 text-gray-700 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-purple-700 lg:p-0 dark:text-gray-400 lg:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white lg:dark:hover:bg-transparent dark:border-gray-700"
                >
                  Usecases
                </a>
              </li>
              <li>
                <a
                  href="#intigration"
                  className="block py-2 pl-3 pr-4 text-gray-700 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-purple-700 lg:p-0 dark:text-gray-400 lg:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white lg:dark:hover:bg-transparent dark:border-gray-700"
                >
                  Integrations
                </a>
              </li>
              <li>
                <a
                  href="#team"
                  className="block py-2 pl-3 pr-4 text-gray-700 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-purple-700 lg:p-0 dark:text-gray-400 lg:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white lg:dark:hover:bg-transparent dark:border-gray-700"
                >
                  Team
                </a>
              </li>
              <li>
                <a
                  href="#faq"
                  className="block py-2 pl-3 pr-4 text-gray-700 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-purple-700 lg:p-0 dark:text-gray-400 lg:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white lg:dark:hover:bg-transparent dark:border-gray-700"
                >
                  FAQ
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
