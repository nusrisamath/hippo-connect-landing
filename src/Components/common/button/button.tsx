import React from "react";

type TButton = {
  label: string,
  variant?: string,
  loading?: boolean,
  type?: "button" | "submit" | "reset" | undefined,
  disabled?: boolean,
  clickFunction?: () => void,
}

const Button: React.FC<TButton> = ({
  label,
  variant,
  loading = false,
  type = "button",
  disabled,
  clickFunction
}) => {
  const getClassBasedOnValue = (value: string) => {
    let className = "";

    switch (value) {
      case "primary":
        className =
          "bg-background-green-neutral text-global-white hover:bg-neutral-600";
        break;
      case "secondary":
        className =
          "border border-green-300 bg-white dark:bg-transparent  text-gray-900 dark:text-gray-300 hover:bg-green-200 hover:text-gray-800";
        break;

      default:
        className =
          "bg-background-green-neutral text-global-white bg-indigo-500 hover:bg-gradient-to-r from-green-500 to-indigo-400";
        break;
    }
    return className;
  };

  return (
    <button
      disabled={disabled}
      type={type}
      onClick={clickFunction}
      className={`${getClassBasedOnValue(
        variant || ""
      )} flex cursor-pointer items-center rounded-[50px] px-6 py-3 font-semibold text-xs md:text-base transition-all duration-500 ${disabled && "opacity-40"
        }`}
    >
      {loading ? <div>loading...</div> : <div>{label}</div>}
    </button>
  );
};

export default Button;
