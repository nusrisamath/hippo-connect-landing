import React from "react";

type TInputCheckBox = {
  label: string;
  value?: string;
  name: string;
}

const InputCheckBox: React.FC<TInputCheckBox> = ({ label = "label", value = "", name = "name" }) => {
  console.log('[value] - ', value);
  return (
    <div className="inline-flex items-center">
      <label
        className="relative flex items-center  rounded-full cursor-pointer mr-2"
        htmlFor={name}
      >
        <input
          type="checkbox"
          className="before:content[''] peer relative h-5 w-5 cursor-pointer appearance-none rounded-md border border-purple-400 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-blue-gray-500 before:opacity-0 before:transition-opacity checked:border-green-400 checked:before:bg-border-green-primary hover:before:opacity-10"
          id={name}
        />
        <span className="absolute text-white transition-opacity duration-700 opacity-0 pointer-events-none top-2/4 left-2/4 -translate-y-2/4 -translate-x-2/4 peer-checked:opacity-100">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-3.5 w-3.5"
            viewBox="0 0 20 20"
            fill="#38fcbb"
            stroke="#38fcbb"
            stroke-width="1"
          >
            <path
              fill-rule="evenodd"
              d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
              clip-rule="evenodd"
            ></path>
          </svg>
        </span>
      </label>
      <label
        className="mt-px font-light text-black  dark:text-white cursor-pointer select-none"
        htmlFor={name}
      >
        {label}
      </label>
    </div>
  );
}

export default InputCheckBox;
