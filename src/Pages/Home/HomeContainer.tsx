import { Faq, GetAccess, HippoConnect, HippoConnectOffers, KeyFeatuesMain, LeaderShipTeam, Requesting, Revolutionise, UseCases } from "./Sections";

const HomeContainer = () => {
  return (
    <>
      <Revolutionise />
      <HippoConnectOffers />
      <KeyFeatuesMain />
      <GetAccess />
      <UseCases />
      <LeaderShipTeam />
      <Requesting />
      <Faq />
      <HippoConnect />
    </>
  );
};

export default HomeContainer;
