import React, { useState } from 'react'

const Faq = () => {
    const [selectedQId, setSelectedQId] = useState(0);
    const faqQ = [
        {
            id: 1,
            quection:
                "Is Smart Data Registry compliant with data protection regulations?",
            answer:
                "Absolutely. We prioritize compliance with global data protection regulations such as FDA and HIPAA, among others. Our commitment to regulatory compliance ensures that your data is handled responsibly and in accordance with the law.",
        },
        {
            id: 2,
            quection: "How does Smart Data Registry ensure data security?",
            answer:
                "Absolutely. We prioritize compliance with global data protection regulations ",
        },
        {
            id: 3,
            quection: "What types of data can be registered on the platform?",
            answer:
                "Absolutely. We prioritize compliance with global data protection regulations ",
        },
        {
            id: 4,
            quection: "How can I contact Physicians and medical clinics?",
            answer:
                "Absolutely. We prioritize compliance with global data protection regulations ",
        },
        {
            id: 5,
            quection:
                "What are the privacy concerns which consumers have when using data from Hippo Connect? ",
            answer:
                "Absolutely. We prioritize compliance with global data protection regulations ",
        },
    ];
    return (
        <section className={`pt-20 bg-gray-200 dark:bg-gray-950 md:h-screen flex justify-center`} id="faq">
            <div className="container px-5">
                <div className="text-gray-700 dark:text-gray-400 text-sm font-normal border-b border-gray-400 dark:border-gray-600 pb-2">
                    FAQ
                </div>
                <div className="pb-7 hidden md:block pt-3">
                    <div className="flex space-x-4 ">
                        <div className="md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                            Frequently asked questions,
                        </div>
                        <div className="text-black  dark:text-white text-[40px] font-medium">
                            by data consumers and buyers when
                        </div>
                    </div>
                    <div className="text-black  dark:text-white text-[40px] font-medium">
                        using the Hippo Connect Data Scheme.
                    </div>
                </div>
                <div className="pb-7 md:hidden block">
                    <div className="pt-5 ">
                        <div className="text-[30px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                            Frequently asked
                        </div>
                        <div className="flex space-x-4">
                            <div className="text-[30px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                                questions,
                            </div>
                            <div className="text-black  dark:text-white text-[30px] font-medium">
                                by data
                            </div>
                        </div>
                        <div className="text-black  dark:text-white text-[30px] font-medium">
                            consumers and buyers when using
                        </div>
                    </div>
                    <div className="text-black  dark:text-white text-[30px] font-medium">
                        the Hippo Connect Data Scheme.
                    </div>
                </div>
                <div className=" md:border md:border-gray-600 md:border-opacity-20 rounded-3xl md:px-10 pt-8 flex flex-col ">
                    {faqQ.map((item) => (
                        <div className="flex flex-col pb-12">
                            <div className="flex justify-between ">
                                <button
                                    className="text-black  dark:text-white text-[20px] font-medium"
                                    onClick={() => setSelectedQId(item.id)}
                                >
                                    {item.quection}
                                </button>
                                <button onClick={() => setSelectedQId(item.id)}>
                                    {selectedQId === item.id ? (
                                        <svg
                                            width="20"
                                            height="20"
                                            viewBox="0 0 20 20"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                d="M13 10H7M19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1C14.9706 1 19 5.02944 19 10Z"
                                                stroke="#22c55e"
                                                stroke-width="2"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                            />
                                        </svg>
                                    ) : (
                                        <svg
                                            width="20"
                                            height="20"
                                            viewBox="0 0 20 20"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                d="M10 7V10M10 10V13M10 10H13M10 10H7M19 10C19 14.9706 14.9706 19 10 19C5.02944 19 1 14.9706 1 10C1 5.02944 5.02944 1 10 1C14.9706 1 19 5.02944 19 10Z"
                                                stroke="#818cf8"
                                                stroke-width="2"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                            />
                                        </svg>
                                    )}{" "}
                                </button>
                            </div>

                            <div
                                className={`transition-opacity duration-1000 ease-in-out  ${selectedQId === item.id ? "opacity-100" : "opacity-0"
                                    }`}
                            >
                                {selectedQId === item.id && (
                                    <div className="text-gray-700  dark:text-gray-400 text-[15px] font-semibold mt-4">
                                        {item.answer}
                                    </div>
                                )}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default Faq