import React from 'react'
// import Frame from "../../../Images/home/getAccess/Image.png";
import docIcon from "../../../Images/home/getAccess/docIcon.png";
import Button from '../../../Components/common/button/button'

const GetAccess = () => {
    return (
        <section className="bg-gray-200 dark:bg-gray-900 md:h-screen/2 -mt-16">
            <div className="h-full w-full bg-[url('./img/uuundulate.svg')] bg-fixed py-5">
                <div className="flex justify-center px-5">
                    <div className="container px-10 py-5 backdrop-blur-sm bg-purple-300/30 rounded-2xl">
                        {/* <div
                            className="h-[800px] w-full  mt-11 flex flex-col justify-between p-4 md:p-16 bg-cover rounded-2xl"
                            style={{
                                backgroundImage: `url(${Frame})`,
                            }}
                        > */}
                        <div className="space-y-4">
                            <div className="text-black  dark:text-white text-[30px] md:text-[40px] font-medium hidden md:block">
                                Get access to verified Medical <br /> Data pool integrated with Ai
                            </div>
                            <div className="text-black  dark:text-white text-[30px] md:text-[40px] font-medium md:hidden block">
                                Get access to <br /> verified <br /> Medical <br /> Data pool
                                <br /> integrated
                                <br /> with Ai
                            </div>
                            <Button label={"Request a access"} />
                        </div>
                        <div className="flex mb-5">
                            <div className="w-1/2 hidden md:block"></div>
                            <div className="md:w-1/2 md:flex items-center border border-border-green-default rounded-3xl bg-blur-3xl md:space-x-6 p-4">
                                <img src={docIcon} />
                                <div className="text-gray-500  dark:text-white text-base font-normal">
                                    Verified among 100K Medical professional and <br /> validated
                                    realtime
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* </div> */}
                </div>
            </div>
        </section>
    )
}

export default GetAccess