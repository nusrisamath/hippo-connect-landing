import React, { useState } from 'react'
import Image from "../../../Images/home/hipppoConnect/Image.png";
import Image1 from "../../../Images/home/hipppoConnect/Image1.png";
import InputCheckBox from '../../../Components/common/inputCheckBox/inputCheckBox';
import Button from '../../../Components/common/button/button';

const HippoConnect = () => {
    const [stateForYourUseCase, setStateForYourUseCase] = useState("");
    return (
        <section className={`pt-20 bg-gray-200 dark:bg-gray-950 md:h-screen`} id="contactUs">
            <div className="h-full w-full bg-[url('./img/sssquiggly.svg')] bg-fixed py-5">
                <div className='flex justify-center'>
                    <div className="container px-5">
                        <div className="text-gray-700 dark:text-gray-400 text-sm font-normal border-b border-gray-400 dark:border-gray-600 pb-2 mb-10">
                            CONNECT US
                        </div>
                        <div className='flex'>


                            <div className="md:w-1/3 pb-8">
                                <div className="flex items-center space-x-2">
                                    <svg
                                        width="34"
                                        height="32"
                                        viewBox="0 0 34 32"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M28.6689 15.1418C27.7197 14.7126 26.7333 14.4659 25.7469 14.3882C24.7031 14.3173 23.8181 13.6583 23.5208 12.7392C23.1898 11.712 23.737 10.83 23.8181 10.7016C23.8181 10.7016 23.8181 10.7016 23.8147 10.7016C23.9498 10.5259 24.0681 10.3333 24.1626 10.1238C24.3653 9.671 24.4464 9.19455 24.416 8.73161C24.2944 7.17047 24.9632 5.72084 26.1658 5.0011C27.1015 4.43679 28.0676 4.49761 28.4594 4.54492C29.4086 4.64291 30.3545 4.13605 30.77 3.21694C31.2868 2.07142 30.7801 0.719787 29.6316 0.202786C28.4865 -0.314215 27.1353 0.192649 26.6184 1.34154C26.5205 1.55442 26.4597 1.77744 26.4326 1.99708C26.3955 2.40258 26.2266 3.61567 25.2368 4.64629C23.8181 6.12296 21.8859 5.99117 21.619 5.96752C20.4503 5.91345 19.3119 6.56224 18.7984 7.69085C18.1296 9.1709 18.7849 10.9145 20.2678 11.5836C20.2881 11.5937 20.3084 11.6005 20.3286 11.6072C20.3286 11.6072 20.3286 11.6072 20.3253 11.6072C21.0752 11.9147 21.6055 12.5973 21.6967 13.388C21.8014 14.2767 21.3353 15.1519 20.5246 15.5912C19.3457 16.2906 18.3255 17.2706 17.5655 18.4904C17.5655 18.4904 17.5655 18.4904 17.5621 18.4904C17.427 18.7067 17.1871 18.852 16.9135 18.852C16.6399 18.852 16.4068 18.7101 16.2683 18.4972C16.2683 18.4972 16.2683 18.4972 16.2649 18.4972C15.5049 17.2773 14.4814 16.294 13.3025 15.5912C12.4917 15.1519 12.029 14.2767 12.1303 13.388C12.2215 12.5973 12.7518 11.9147 13.5018 11.6072C13.5018 11.6072 13.5018 11.6072 13.4984 11.6072C13.5186 11.5971 13.5389 11.5937 13.5592 11.5836C15.0387 10.9145 15.6974 9.1709 15.0286 7.69085C14.5185 6.55886 13.3801 5.91007 12.208 5.96752C11.9378 5.99117 10.0056 6.11958 8.59018 4.64629C7.60043 3.61905 7.43153 2.40595 7.39437 1.99708C7.36735 1.77744 7.30655 1.55442 7.20859 1.34154C6.70527 0.192649 5.35408 -0.314215 4.20894 0.202786C3.06381 0.719787 2.55373 2.07142 3.07056 3.21694C3.48606 4.13267 4.43189 4.64291 5.3811 4.54492C5.77632 4.49761 6.73905 4.43679 7.67475 5.0011C8.87393 5.72422 9.54615 7.17385 9.42454 8.73161C9.39751 9.19455 9.47521 9.671 9.67789 10.1238C9.77247 10.3333 9.8907 10.5259 10.0258 10.7016C10.0258 10.7016 10.0258 10.7016 10.0224 10.7016C10.1035 10.8267 10.6541 11.712 10.3197 12.7392C10.0224 13.6583 9.14079 14.3173 8.09362 14.3882C7.11062 14.4693 6.12426 14.716 5.17167 15.1451C0.746517 17.1456 -1.21946 22.3663 0.783675 26.7962C2.78681 31.2296 8.00241 33.1962 12.4343 31.1924C14.1976 30.395 15.5657 29.0839 16.4507 27.516C16.4507 27.516 16.4541 27.5194 16.4575 27.5227C16.5521 27.3673 16.7243 27.2592 16.9236 27.2592C17.1229 27.2592 17.2986 27.3707 17.3932 27.5329C17.3932 27.5329 17.3999 27.5329 17.4033 27.5295C18.2917 29.0873 19.653 30.395 21.413 31.1891C25.8449 33.1929 31.0605 31.2262 33.0636 26.7929C35.0668 22.3595 33.1008 17.1422 28.6689 15.1384V15.1418Z"
                                            fill="#15928F"
                                        />
                                    </svg>

                                    <div className="text-[30px] md:text-[40px] font-medium text-black  dark:text-white">
                                        Hippo Connect
                                    </div>
                                </div>
                                <div className="text-[15px] md:text-[16px] font-medium text-black  dark:text-white">
                                    Feel free to reach out to our team how could provide medical data and
                                    earn. We eagerly await the opportunity to assist you.
                                </div>
                            </div>
                            <div className='md:w-2/3 backdrop-blur-sm bg-white/30 px-5 py-10 rounded-2xl'>
                                <div className="flex flex-col md:flex-row space-y-12 md:space-y-0 md:space-x-12">
                                    <div className="md:w-1/2 ">
                                        <input
                                            type="text"
                                            className="rounded-2xl px-4 py-2 border-b border-purple-400 placeholder-gray-400 text-lg font-normal focus:outline-none focus:border-green-400 w-full text-black  dark:text-white"
                                            placeholder="Full Name *"
                                        />{" "}
                                    </div>
                                    <div className="md:w-1/2">
                                        <input
                                            type="email"
                                            className="rounded-2xl px-4 py-2 border-b border-purple-400 placeholder-gray-400 text-lg font-normal focus:outline-none focus:border-green-400 w-full text-black  dark:text-white"
                                            placeholder="Email * "
                                        />{" "}
                                    </div>
                                </div>

                                <div className="pt-10">
                                    <div className="text-black  dark:text-white font-semibold text-lg">
                                        Whats your usecase
                                    </div>
                                    <div className="flex flex-col md:flex-row space-y-4 md:space-y-0 pt-2 md:space-x-4">
                                        <div
                                            className="md:w-1/2 "
                                            role="button"
                                            onClick={() =>
                                                setStateForYourUseCase("I_want_access_medical_database")
                                            }
                                        >
                                            <div
                                                className={`border-2 border-purple-400 border-opacity-20 rounded-lg p-4 flex space-x-4 items-center transition-all duration-500 hover:shadow-md hover:shadow-purple-400 ${stateForYourUseCase === "I_want_access_medical_database" &&
                                                    "border-green-400"
                                                    }`}
                                            >
                                                {" "}
                                                <img src={Image} className="w-[40px] h-[40px]" />
                                                <div className="flex justify-between space-x-2 w-full">
                                                    <div className="font-medium  text-sm text-black  dark:text-white">
                                                        I want access medical database
                                                        <div className=" text-sm font-normal text-gray-400  ">
                                                            Sample description
                                                        </div>
                                                    </div>
                                                    {stateForYourUseCase === "I_want_access_medical_database" ? (
                                                        <div className="rounded-full bg-green-400  h-4 w-4 flex  justify-center items-center transition-all duration-700">
                                                            <div className="border border-purple-400  rounded-full   h-2 w-2 flex  justify-center items-center"></div>
                                                        </div>
                                                    ) : (
                                                        <div className="rounded-full border border-border-web h-4 w-4 transition-all duration-700"></div>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            className="md:w-1/2 "
                                            role="button"
                                            onClick={() =>
                                                setStateForYourUseCase("I_want_upload_patient_data")
                                            }
                                        >

                                            <div
                                                className={`border-2 border-purple-400 border-opacity-20 rounded-lg p-4 flex space-x-4 items-center transition-all duration-500 hover:shadow-md hover:shadow-purple-400 ${stateForYourUseCase === "I_want_upload_patient_data" &&
                                                    "border-green-400"
                                                    }`}
                                            >
                                                {" "}
                                                <img src={Image1} className="w-[40px] h-[40px]" />
                                                <div className="flex justify-between space-x-2 w-full">
                                                    <div className="font-medium text-black  dark:text-white text-sm">
                                                        I want upload patient data{" "}
                                                        <div className="text-gray-400 text-sm font-normal">
                                                            Sample description
                                                        </div>
                                                    </div>
                                                    {stateForYourUseCase === "I_want_upload_patient_data" ? (
                                                        <div className="rounded-full bg-green-400  h-4 w-4 flex  justify-center items-center transition-all duration-700">
                                                            <div className="border border-purple-400  rounded-full   h-2 w-2 flex  justify-center items-center"></div>
                                                        </div>
                                                    ) : (
                                                        <div className="rounded-full border border-border-web h-4 w-4 transition-all duration-700"></div>
                                                    )}
                                                </div>
                                            </div>
                                        </div>{" "}
                                    </div>
                                </div>

                                <div className="pt-10">
                                    <div className="text-black  dark:text-white font-semibold text-lg">
                                        Facility type{" "}
                                    </div>
                                    <div className="md:flex pt-2 md:space-x-4 ">
                                        <div className="md:w-1/3 space-y-2">
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="General Hospitals"
                                                    name="general_Hospitals"
                                                />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Diagnostic Center"
                                                    name="diagnostic_center"
                                                />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Psychiatric Hospitals"
                                                    name="psychiatric_hospitals"
                                                />
                                            </div>
                                        </div>
                                        <div className="md:w-1/3 space-y-2">
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Specialty Hospitals"
                                                    name="specialty_hospitals"
                                                />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox label="Surgical Center" name="surgical_center" />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Urgent Care Center"
                                                    name="urgent_care_center"
                                                />
                                            </div>
                                        </div>
                                        <div className="md:w-1/3 space-y-2">
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Rehabilitation Hospitals"
                                                    name="rehabilitation_hospitals"
                                                />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Long-Term Acute Care Hospitals (LTACHs)"
                                                    name="long_term_acute_care_hospitals"
                                                />
                                            </div>
                                            <div className="flex space-x-2">
                                                <InputCheckBox
                                                    label="Hospice and Palliative Care Centres"
                                                    name="hospice_and_palliative_care_centres"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="pt-8">
                                    <textarea
                                        // type="text"
                                        className="border-b h-32 rounded-2xl px-4 pt-2 border-purple-400 placeholder-neutral-400 text-lg font-normal focus:outline-none focus:border-green-400 pb-1 w-full"
                                        placeholder="Your Message *"
                                    />{" "}
                                </div>
                                <div className="flex justify-between pt-12">
                                    <div>
                                        <Button label={"Send Message"} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HippoConnect