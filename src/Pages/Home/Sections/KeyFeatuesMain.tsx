import React, { useRef, useState } from "react";
import Slider from "react-slick";
import KeyFeatures from "./keyFeatures";

import pMIcon3 from "../../../Images/home/key/practiceManagement/pMIcon3.png";
import pMIcon4 from "../../../Images/home/key/practiceManagement/pMIcon4.png";
import pMIcon5 from "../../../Images/home/key/practiceManagement/pMIcon5.png";
import Icon6 from "../../../Images/home/key/hippo/Icon6.png";
import Icon7 from "../../../Images/home/key/hippo/Icon7.png";

import logo1 from "../../../Images/home/key/keyFeature1.png";
import logo2 from "../../../Images/home/key/keyFeature2.png";
import logo3 from "../../../Images/home/key/keyFeature3.png";
import Frame2 from "../../../Images/home/key/Frame2.png";
import Frame3 from "../../../Images/home/key/Frame3.png";

interface KeyFeature {
    id: number;
    topic: string;
    desc: string;
    svg: string;
}

interface State {
    slideIndex: number;
    updateCount: number;
}

const KeyFeatuesMain: React.FC = () => {
    const hippoConnectAi: KeyFeature[] = [
        {
            id: 1,
            topic: "Effortless AI-Powered Search",
            desc: "The platform allows for the effortless search of deidentified healthcare data across various specialties and disciplines. It offers a one-stop solution with hands-free, natural language AI for data queries.",
            svg: logo3,
        },
        {
            id: 2,
            topic: "Smart Data Registry",
            desc: "HippoConnect provides a centralized data registry and marketplace with longitudinal data streaming capabilities. It allows for continual prospective curation and alerts users as flagged data enters the marketplace.",
            svg: logo1,
        },
        {
            id: 3,
            topic: "Smart Chart Builder Pro",
            desc: "This feature enables users to build complex charts quickly, regardless of the size or complexity of the data set, supporting research and analysis.",
            svg: logo2,
        },
    ];
    const practiceManagementSuite: KeyFeature[] = [
        {
            id: 1,
            topic: "Smart Claim Management System",
            desc: "This system integrates practice operations with clearinghouses or billing companies, ensuring real-time claim creation and reconciliation.",
            svg: pMIcon3,
        },
        {
            id: 2,
            topic: "Smart Data Registry",
            desc: "A tool designed to address insurance claim denials and facilitate the creation of comprehensive Letters of Medical Necessity.",
            svg: pMIcon4,
        },
        {
            id: 3,
            topic: "Smart Chart Builder Pro",
            desc: "This feature modernizes patient-practice interactions, enhancing patient experience with easy-to-use tools and a secure patient portal.",
            svg: pMIcon5,
        },
    ];

    const [state, setState] = useState<State>({
        slideIndex: 0,
        updateCount: 0,
    });
    const sliderRef = useRef<Slider | null>(null);

    const topicGenerator = (): {
        gradientTopic: string;
        topTopic: string;
        secondTopic: string;
    } => {
        if (state.slideIndex === 0) {
            return {
                gradientTopic: "Hippo Connect Ai",
                topTopic: " upgraded with features to access from",
                secondTopic: "structured data pool",
            };
        } else if (state.slideIndex === 1) {
            return {
                gradientTopic: "Practice Management Suite",
                topTopic: "allows to capture multi modal",
                secondTopic: "medical management datasets",
            };
        } else if (state.slideIndex === 2) {
            return {
                gradientTopic: "Hippo Connect",
                topTopic: "comes with built in security features that",
                secondTopic: "support multi channel usecases",
            };
        }
        return {
            gradientTopic: "",
            topTopic: "",
            secondTopic: ""
        };
    };
    const hippoConnect: KeyFeature[] = [
        {
            id: 1,
            topic: "Bulletproof Security standards",
            desc: "HippoConnect adheres to FHIR (Fast Healthcare Interoperability Resources) standards and HIPAA/HITECH regulatory guidelines. It ensures data encryption, maintains audit trails, and is ONC-ATCB Certified.",
            svg: Icon6,
        },
        {
            id: 2,
            topic: "Flexible Pricing & Revenue Models",
            desc: "The platform offers various pricing and financial models for its users, including physician member fee schedules, organisation fee schedules, and a revenue-share model for external data monetisation transactions.",
            svg: Icon7,
        },
    ];

    const settings = {
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        beforeChange: (current: number, next: number) => setState({ slideIndex: next, updateCount: state.updateCount + 1 }),
    };
    const handleWheelScroll = (e: React.WheelEvent<HTMLDivElement>) => {
        const delta = e.deltaY;

        if (sliderRef.current) {
            if (delta > 0) {
                // Scrolling down, move to the next slide
                sliderRef.current.slickNext();
            } else if (delta < 0) {
                // Scrolling up, move to the previous slide
                sliderRef.current.slickPrev();
            }
        }
    };

    return (
        <section className={` bg-gray-200 dark:bg-gray-900 md:h-screen flex justify-center`} id="features">
            <div className="container px-5 pt-20">
                <div className="text-gray-700 dark:text-gray-400 text-sm font-normal border-b border-gray-400 dark:border-gray-600 pb-2">
                    KEY FEATURES
                </div>
                <div className="pb-7">
                    <div className="md:flex md:space-x-4 pt-5 ">
                        <div className="text-4xl md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                            {topicGenerator().gradientTopic}
                        </div>
                        <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
                            {topicGenerator().topTopic}
                        </div>
                    </div>
                    <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium transition-all duration-700">
                        {topicGenerator().secondTopic}
                    </div>
                </div>
                <div className=" md:border md:border-gray-600 md:border-opacity-20 rounded-3xl md:p-9  ">
                    <div className=" flex justify-end pb-2">
                        <div className="flex flex-col pb-5 space-y-3 items-end">
                            <button
                                onClick={() => sliderRef?.current?.slickGoTo(0)}
                                className={`h-1  ${state.slideIndex === 0
                                    ? "bg-green-400 w-16"
                                    : "bg-purple-900  w-8"
                                    } transition-all duration-1000 `}
                            />
                            <button
                                onClick={() => sliderRef?.current?.slickGoTo(1)}
                                className={`h-1  ${state.slideIndex === 1
                                    ? "bg-green-400 w-16"
                                    : "bg-purple-900  w-8"
                                    } transition-all duration-1000 `}
                            />
                            <button
                                onClick={() => sliderRef?.current?.slickGoTo(2)}
                                className={`h-1  ${state.slideIndex === 2
                                    ? "bg-green-400 w-16"
                                    : "bg-purple-900 w-8"
                                    } transition-all duration-1000 `}
                            />
                        </div>
                    </div>
                    <div onWheel={handleWheelScroll}>
                        <Slider ref={sliderRef} {...settings}>
                            <div className="container">
                                <KeyFeatures keyFeatures={hippoConnectAi} />
                            </div>
                            <div className="">
                                <KeyFeatures
                                    keyFeatures={practiceManagementSuite}
                                    rightSideImage={Frame2}
                                />{" "}
                            </div>
                            <div className="">
                                <KeyFeatures keyFeatures={hippoConnect} rightSideImage={Frame3} />
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default KeyFeatuesMain;
