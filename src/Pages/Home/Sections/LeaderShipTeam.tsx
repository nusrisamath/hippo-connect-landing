import React from 'react'
import autoLayoutVertical from "../../../Images/home/leadershipTeam/autoLayoutVertical.png";
import img from "../../../Images/home/leadershipTeam/img.png";
import img1 from "../../../Images/home/leadershipTeam/img1.png";
import img2 from "../../../Images/home/leadershipTeam/img2.png";
import img4 from "../../../Images/home/leadershipTeam/img4.png";
import applied from "../../../Images/home/leadershipTeam/tags/applied.png";
import assistmd from "../../../Images/home/leadershipTeam/tags/assistmd.png";
import cardinal from "../../../Images/home/leadershipTeam/tags/cardinal.png";
import coviden from "../../../Images/home/leadershipTeam/tags/coviden.png";
import credibl from "../../../Images/home/leadershipTeam/tags/credibl.png";
import lableo from "../../../Images/home/leadershipTeam/tags/lableo.png";
import mammh from "../../../Images/home/leadershipTeam/tags/mammh.png";
import carium from "../../../Images/home/leadershipTeam/tags/carium.png";
import greenway from "../../../Images/home/leadershipTeam/tags/greenway.png";
import allscripts from "../../../Images/home/leadershipTeam/tags/allscripts.png";
import microsoft from "../../../Images/home/leadershipTeam/tags/microsoft.png";
import philips from "../../../Images/home/leadershipTeam/tags/philips.png";
import them from "../../../Images/home/leadershipTeam/tags/them.png";
import medcap from "../../../Images/home/leadershipTeam/tags/medcap.png";
import merck from "../../../Images/home/leadershipTeam/tags/merck.png";
import stjude from "../../../Images/home/leadershipTeam/tags/stjude.png";

const LeaderShipTeam = () => {

    const perosnsArray = [
        {
            id: 1,
            name: "Daniel Baker",
            subTopic: "CTO",
            desc: "Board member at Mammoth and AssistMD.",
            tags: [
                { url: mammh, name: "mammh", classname: "" },
                { url: assistmd, name: "assistmd", classname: "" },
            ],
            profilePic: img,
        },
        {
            id: 2,
            name: "Jeff Stern",
            subTopic: "VP Strategic Partnerships",
            desc: "Board member at Carium, Greenway Health, Allscript and Philips.",
            tags: [
                { url: carium, name: "carium", classname: "" },
                { url: greenway, name: "greenway", classname: "" },
                { url: allscripts, name: "allscripts", classname: "" },
                { url: philips, name: "philips", classname: "" },
            ],
            profilePic: img1,
        },
        {
            id: 3,
            name: "Andy Schlagel",
            subTopic: "CCO",
            desc: "Board member at CardinalHealth, Applied Medical, Covidien and Microsoft.",
            tags: [
                { url: coviden, name: "coviden", classname: "" },
                { url: microsoft, name: "microsoft", classname: "" },
                { url: applied, name: "applied", classname: "" },
                { url: cardinal, name: "cardinal", classname: "" },
            ],
            profilePic: img2,
        },
        {
            id: 4,
            name: "Chad Adams",
            subTopic: "VP Sales",
            desc: "Board member at Medcorp LLC, THEM, St. Jude Medical and Merck.",
            tags: [
                { url: them, name: "coviden", classname: "" },
                { url: medcap, name: "medcap", classname: "" },
                { url: merck, name: "merck", classname: "" },
                { url: stjude, name: "stjude", classname: "" },
            ],
            profilePic: img4,
        },
    ];

    return (
        <section className={`pt-20 bg-gray-200 dark:bg-gray-950 md:h-screen flex justify-center`} id="team">
            <div className="container px-5 mb-10">
                <div className="text-gray-700 dark:text-gray-400 text-sm font-normal border-b border-gray-400 dark:border-gray-600 pb-2">
                    TEAM MEMBERS
                </div>
                <div className="pb-7">
                    <div className="text-black  dark:text-white text-[40px] font-medium pt-3">
                        Get to know the dedicated individuals driving our vision.
                    </div>
                    <div className="md:flex md:space-x-4 -mt-2">
                        <div className="text-4xl md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                            Meet the team
                        </div>
                        <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
                            behind our innovation & excellence.
                        </div>
                    </div>
                </div>
                <div className="flex flex-col md:flex-row gap-8 mt-10">
                    <div className="md:w-1/3 md:pr-20">
                        <div className="border border-opacity-20 rounded-3xl border-gray-600 transition-all duration-700 hover:scale-20 bg-white dark:bg-gray-800 hover:shadow-2xl hover:shadow-purple-900 shadow-md shadow-purple-700">
                            <div className="flex flex-col justify-between">
                                <div className="h-1/2">
                                    <img
                                        className="w-full h-full rounded-3xl"
                                        alt="Rayan Hilton"
                                        src={autoLayoutVertical}
                                    />
                                </div>

                                <div className="h-1/2 p-8">
                                    <div className="bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text text-xl font-medium">
                                        Rayan Hilton
                                    </div>{" "}
                                    <div className="text-sm font-normal text-text-subtle text-black dark:text-white pb-2">
                                        Founder & CEO
                                    </div>
                                    <div className="pb-2">
                                        <svg
                                            width="20"
                                            height="20"
                                            viewBox="0 0 20 20"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="cursor-pointer"
                                        >
                                            <path
                                                d="M18.5195 0H1.47656C0.660156 0 0 0.644531 0 1.44141V18.5547C0 19.3516 0.660156 20 1.47656 20H18.5195C19.3359 20 20 19.3516 20 18.5586V1.44141C20 0.644531 19.3359 0 18.5195 0ZM5.93359 17.043H2.96484V7.49609H5.93359V17.043ZM4.44922 6.19531C3.49609 6.19531 2.72656 5.42578 2.72656 4.47656C2.72656 3.52734 3.49609 2.75781 4.44922 2.75781C5.39844 2.75781 6.16797 3.52734 6.16797 4.47656C6.16797 5.42188 5.39844 6.19531 4.44922 6.19531ZM17.043 17.043H14.0781V12.4023C14.0781 11.2969 14.0586 9.87109 12.5352 9.87109C10.9922 9.87109 10.7578 11.0781 10.7578 12.3242V17.043H7.79688V7.49609H10.6406V8.80078H10.6797C11.0742 8.05078 12.043 7.25781 13.4844 7.25781C16.4883 7.25781 17.043 9.23438 17.043 11.8047V17.043Z"
                                                fill="#475569"
                                            />
                                        </svg>
                                    </div>
                                    <div className="text-sm font-normal text-text-subtle text-black dark:text-white">
                                        Board member at Mammoth,Lab Leopard and Credibl.{" "}
                                    </div>
                                    <div className="flex">
                                        <div>
                                            {" "}
                                            <img
                                                className={`grayscale pr-4 py-2 `}
                                                src={mammh}
                                                alt="assistmd"
                                            />
                                        </div>
                                        <div>
                                            {" "}
                                            <img
                                                className={`grayscale pr-4 py-2 `}
                                                src={credibl}
                                                alt="assistmd"
                                            />
                                        </div>
                                        <div>
                                            {" "}
                                            <img
                                                className={`grayscale pr-4 py-2 `}
                                                src={lableo}
                                                alt="assistmd"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="md:w-2/3">
                        <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
                            {perosnsArray.map((item) => (
                                <div className="border border-opacity-20 rounded-3xl border-gray-600 transition-all duration-700 hover:scale-20 bg-white dark:bg-gray-800 hover:shadow-2xl hover:shadow-purple-900 shadow-md shadow-purple-700 p-8">
                                    <div className="flex">
                                        <div className="pr-4 pb-4">
                                            <img
                                                className=" w-[100px] h-[100px] rounded-xl"
                                                src={item.profilePic}
                                                alt={item.name}
                                            />
                                        </div>

                                        <div className="">
                                            <div className="bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text text-xl font-medium pb-2">
                                                {item.name}
                                            </div>{" "}
                                            <div className="text-sm font-normal text-text-subtle pb-5 text-black dark:text-white">
                                                {item.subTopic}
                                            </div>
                                            <div>
                                                <svg
                                                    width="20"
                                                    height="20"
                                                    viewBox="0 0 20 20"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    className="cursor-pointer"
                                                >
                                                    <path
                                                        d="M18.5195 0H1.47656C0.660156 0 0 0.644531 0 1.44141V18.5547C0 19.3516 0.660156 20 1.47656 20H18.5195C19.3359 20 20 19.3516 20 18.5586V1.44141C20 0.644531 19.3359 0 18.5195 0ZM5.93359 17.043H2.96484V7.49609H5.93359V17.043ZM4.44922 6.19531C3.49609 6.19531 2.72656 5.42578 2.72656 4.47656C2.72656 3.52734 3.49609 2.75781 4.44922 2.75781C5.39844 2.75781 6.16797 3.52734 6.16797 4.47656C6.16797 5.42188 5.39844 6.19531 4.44922 6.19531ZM17.043 17.043H14.0781V12.4023C14.0781 11.2969 14.0586 9.87109 12.5352 9.87109C10.9922 9.87109 10.7578 11.0781 10.7578 12.3242V17.043H7.79688V7.49609H10.6406V8.80078H10.6797C11.0742 8.05078 12.043 7.25781 13.4844 7.25781C16.4883 7.25781 17.043 9.23438 17.043 11.8047V17.043Z"
                                                        fill="#475569"
                                                    />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div className=" text-xs font-normal text-text-subtle text-black dark:text-white">
                                        {item.desc}
                                    </div>
                                    <div className="flex flex-wrap overflow-auto ">
                                        {item.tags.map((item) => (
                                            <img
                                                className={`grayscale pr-4 py-2 ${item.classname}`}
                                                src={item.url}
                                                alt={item.name}
                                            />
                                        ))}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default LeaderShipTeam