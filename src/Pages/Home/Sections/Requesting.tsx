import React from 'react'
import MaskGroups from "../../../Images/home/experienceTheFutureOfHealthcare/MaskGroups.png";
import Button from '../../../Components/common/button/button'

const Requesting = () => {
    return (
        <section className="bg-gray-200 dark:bg-gray-900 md:h-screen -mt-16">
            <div className="h-full w-full bg-[url('./img/bbburst.svg')] bg-fixed py-5">
                <div className="flex justify-center px-5">
                    <div
                        className="h-[800px] container  mt-11 flex flex-col justify-between p-4 md:p-16 bg-cover rounded-2xl"
                        style={{
                            backgroundImage: `url(${MaskGroups})`,
                        }}
                    >
                        <div className="space-y-4">
                            <div className="text-black  dark:text-white text-[30px] md:text-[40px] font-medium hidden md:block">
                                Experience the Future of<br />
                                Healthcare Data Management
                            </div>
                            <div className="text-black  dark:text-white text-[30px] md:text-[40px] font-medium md:hidden block">
                                Experience <br /> the <br /> Future of
                                <br /> Healthcare
                                <br /> Data Management
                            </div>
                            <Button label={"Request a access"} />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Requesting