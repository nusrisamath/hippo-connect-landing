// import Tags from "../../../Images/home/tags.png";
// import Tags2 from "../../../Images/home/tags2.png";
import Frame2 from "../../../Images/home/revolutionise/frame2.png";
import Profile from "../../../Images/home/revolutionise/profileImgs/profile.png";
import profile1 from "../../../Images/home/revolutionise/profileImgs/profile1.png";
import profile2 from "../../../Images/home/revolutionise/profileImgs/profile2.png";
import profile3 from "../../../Images/home/revolutionise/profileImgs/profile3.png";
import Button from "../../../Components/common/button/button";
import CompanyLogo from "./companyLogo";

const Revolutionise = () => {
  return (
    <section className="bg-gray-200 dark:bg-gray-900 h-screen -mt-16">
      <div className="h-full w-full bg-[url('./img/wwwhirl.svg')] bg-cover">
        <div className="flex justify-center">
          <div className="container px-5 pt-36">
            <div className="grid md:grid-cols-2">
              <div className="">
                <div className="flex items-center space-x-4 mb-2 md:mb-6">
                  <div className="text-[30px] md:text-[40px] font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl text-black  dark:text-white">
                    Revolutionise
                  </div>
                </div>
                <div className="flex items-center space-x-2 md:space-x-4 -mt-1 mb-2 md:mb-6">
                  <div className="text-[30px] md:text-[40px]  font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl text-black  dark:text-white">
                    Healthcare
                  </div>
                  <div className="text-[30px] md:text-[40px]  font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl text-black  dark:text-white">
                    Data
                  </div>
                </div>
                <div className="flex space-x-4 mb-2 md:mb-6">
                  <div className="text-[30px] md:text-[40px] font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl text-black  dark:text-white">
                    Management
                  </div>
                  <div className="text-[30px] md:text-[40px] font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl text-black  dark:text-white">
                    with
                  </div>
                </div>
                <div className="flex items-center space-x-4 mb-2 md:mb-6">
                  <div className="text-[30px] md:text-[40px] font-semibold leading-none tracking-tight md:text-5xl xl:text-6xl bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                    Hippo Connect
                  </div>
                </div>
                <div className="flex mb-2 md:mb-6 md:space-x-4 flex-col md:flex-row">
                  <div className="flex md:justify-center">
                    <div className="flex items-center">
                      <img
                        src={profile1}
                        alt="Profile"
                        className="w-8 h-8 rounded-full border-2 border-green-500"
                      />
                      <img
                        src={profile3}
                        alt="Profile"
                        className="w-8 h-8 rounded-full border-2 border-purple-500 -ml-3 "
                      />
                      <img
                        src={profile2}
                        alt="Profile"
                        className="w-8 h-8 rounded-full border-2 border-green-500 -ml-3"
                      />
                      <img
                        src={Profile}
                        alt="Profile"
                        className="w-8 h-8 rounded-full border-2 border-purple-500 -ml-3"
                      />

                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="21"
                        height="21"
                        viewBox="0 0 21 21"
                        fill="none"
                        className="-mr-2"
                      >
                        <path
                          fill-rule="evenodd"
                          clip-rule="evenodd"
                          d="M8.24504 14.962C7.85452 14.5715 7.85452 13.9383 8.24504 13.5478L11.5379 10.2549L8.24504 6.96199C7.85452 6.57147 7.85452 5.9383 8.24504 5.54778C8.63557 5.15725 9.26873 5.15725 9.65926 5.54778L13.6593 9.54778C14.0498 9.9383 14.0498 10.5715 13.6593 10.962L9.65926 14.962C9.26873 15.3525 8.63557 15.3525 8.24504 14.962Z"
                          fill="#66CCA1"
                        />
                      </svg>
                    </div>
                  </div>
                </div>

                <p className="max-w-2xl mb-6 font-light text-gray-600 lg:mb-8 md:text-lg lg:text-xl dark:text-gray-400">
                  Connecting physicians, medical clinics, and consumers seamlessly –
                  your comprehensive healthcare ecosystem for a healthier future.
                </p>
                <div className="space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
                  <div className="">
                    <Button label="Request a Demo" variant={""} loading={false} type={undefined} disabled={false} clickFunction={function (): void {
                      throw new Error("Function not implemented.");
                    }} />
                  </div>
                  <div>
                    <Button label="Learn more" variant="secondary" />
                  </div>
                </div>
              </div>
              <div className="justify-center items-center hidden lg:flex">
                <div className="hidden lg:mt-0 lg:col-span-5 lg:flex md:w-2/3">
                  <img src={Frame2} alt="hippo connect hero" />
                </div>
              </div>
            </div>
          </div>

        </div>
        <div className="bg-white dark:bg-gray-900 opacity-95 py-2 mt-10 md:py-16 md:mt-28">
          <CompanyLogo />
        </div>
      </div>
    </section >
  );
};

export default Revolutionise;
