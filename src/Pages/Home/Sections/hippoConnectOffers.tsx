import React from "react";
import Button from "../../../Components/common/button/button";

const HippoConnectOffers = () => {
  const bulletData = [
    { id: 1, title: "Multi EMR connectivity support" },
    { id: 2, title: "High security and compliance standards." },
    { id: 3, title: "Automations with third-party applications" },
  ];
  const stepperData = [
    {
      id: 1,
      topic: "Step 1",
      description:
        "Physicians complete Authorisation related paperwork after contacting sales team.",
      imageLink: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="25"
          height="24"
          viewBox="0 0 25 24"
          fill="none"
        >
          <path
            d="M15.5 19C16.6046 19 17.5 18.1046 17.5 17C17.5 15.8954 16.6046 15 15.5 15H10.5C9.39543 15 8.5 14.1046 8.5 13C8.5 12.2597 8.9022 11.6134 9.5 11.2676M12.5 19H10.5M15.5 11H17.5C19.1569 11 20.5 9.65685 20.5 8C20.5 6.34315 19.1569 5 17.5 5H8.5M12.5 3V21M8.5 5H6.5C5.39543 5 4.5 5.89543 4.5 7C4.5 8.10457 5.39543 9 6.5 9C7.60457 9 8.5 8.10457 8.5 7V6.5V5Z"
            stroke="#836DF2"
            stroke-width="1.995"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      ),
    },
    {
      id: 1,
      topic: "Step 2",
      description:
        "Hippo Connect expose API end-points to Physicians EMR & Extracts De-identified data.",
      imageLink: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="29"
          height="29"
          viewBox="0 0 29 29"
          fill="none"
        >
          <path
            d="M16.6112 24.1113C16.6112 25.4 15.5665 26.4447 14.2778 26.4447C12.9892 26.4447 11.9445 25.4 11.9445 24.1113M16.6112 24.1113C16.6112 22.8227 15.5665 21.778 14.2778 21.778M16.6112 24.1113H24.7778M11.9445 24.1113C11.9445 22.8227 12.9892 21.778 14.2778 21.778M11.9445 24.1113H3.77783M14.2778 21.778V17.1113M24.7778 6.61133C24.7778 8.54433 20.0768 10.1113 14.2778 10.1113C8.47884 10.1113 3.77783 8.54433 3.77783 6.61133M24.7778 6.61133C24.7778 4.67833 20.0768 3.11133 14.2778 3.11133C8.47884 3.11133 3.77783 4.67833 3.77783 6.61133M24.7778 6.61133V13.6113C24.7778 15.548 20.1112 17.1113 14.2778 17.1113M3.77783 6.61133V13.6113C3.77783 15.548 8.4445 17.1113 14.2778 17.1113"
            stroke="#836DF2"
            stroke-width="2.36444"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      ),
    },
    {
      id: 3,
      topic: "Step 3",
      description:
        "Earn based on commission basis based on the usage of datasets which you have exposed with Hippo Connect.",
      imageLink: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="29"
          height="29"
          viewBox="0 0 29 29"
          fill="none"
        >
          <path
            d="M25.9444 12.4445H2.61108M13.1111 22.9445L22.2111 22.9445C23.5179 22.9445 24.1713 22.9445 24.6704 22.6902C25.1094 22.4665 25.4664 22.1095 25.6901 21.6705C25.9444 21.1713 25.9444 20.518 25.9444 19.2112V10.3445C25.9444 9.03771 25.9444 8.38432 25.6901 7.88519C25.4664 7.44614 25.1094 7.08919 24.6704 6.86549C24.1713 6.61117 23.5179 6.61117 22.2111 6.61117H20.1111M13.1111 22.9445L15.4444 25.2778M13.1111 22.9445L15.4444 20.6112M8.44442 22.9445H6.34442C5.03763 22.9445 4.38423 22.9445 3.88511 22.6902C3.44606 22.4665 3.08911 22.1095 2.8654 21.6705C2.61108 21.1713 2.61108 20.518 2.61108 19.2112V10.3445C2.61108 9.03771 2.61108 8.38431 2.8654 7.88519C3.08911 7.44614 3.44606 7.08919 3.88511 6.86548C4.38423 6.61117 5.03763 6.61117 6.34442 6.61117H15.4444M15.4444 6.61117L13.1111 8.9445M15.4444 6.61117L13.1111 4.27783"
            stroke="#836DF2"
            stroke-width="2.36444"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      ),
    },
  ];
  return (
    <section className="bg-gradient-to-b to-white from-gray-200 dark:bg-gradient-to-b dark:from-gray-900 dark:to-gray-950 md:h-screen flex justify-center" id="intigration">
      <div className="container px-5 mt-16">
        <div className="md:flex py-9">
          <div className="md:w-3/5">
            <div className="pb-6">
              <div className="md:flex md:space-x-4">
                <div className="text-[30px] md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                  Hippo Connect
                </div>{" "}
                <div className="text-[30px] md:text-[40px] font-medium text-black  dark:text-white">
                  Offers seamless
                </div>{" "}
              </div>
              <div className="text-[30px] md:text-[40px] font-medium text-black  dark:text-white">
                connectivity to 100+ EMRs,{" "}
              </div>
              <div className="text-[30px] md:text-[40px] font-medium md:flex md:space-x-4 text-black  dark:text-white">
                <div className="">secure</div>
                <div>de-identified data</div>
                <div>ingestion.</div>
              </div>
            </div>
            <div className="pb-4 text-gray-600 lg:mb-8 dark:text-gray-400">
              Secure interoperable application programming interface (API)
              connecting electronic medical records (EMR) systems with third-party
              applications.
            </div>
            <Button label={"Partner with Hippo Connect"} />
          </div>

          <div className="md:w-2/5 pt-12 md:pt-3">
            <div className="flex flex-col space-y-4">
              {bulletData.map((item) => (
                <div className="flex items-center space-x-3 " key={item.id}>
                  <div className="flex items-center justify-center h-8 w-8 rounded-full bg-gradient-to-b to-green-500 from-indigo-400 shadow-2xl">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      viewBox="0 0 16 16"
                      fill="none"
                    >
                      <path
                        d="M3.33325 8.6665L5.99992 11.3332L12.6666 4.6665"
                        stroke="white"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                  </div>
                  <div className="font-normal text-base text-gray-600">
                    {item.title}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="md:flex  md:space-x-4 space-y-4 md:space-y-0 pt-12 md:pt-0 ">
          {stepperData.map((item) => (
            <div className=" w-full border border-opacity-20 rounded-2xl border-gray-600 px-10 py-8 transition-all duration-700 hover:scale-20 bg-white dark:bg-gray-800 hover:shadow-2xl hover:shadow-green-900 shadow-md shadow-green-700">
              <div className="flex justify-center items-center dark:bg-gray-900 bg-purple-100 rounded-xl w-16 h-16">
                {item.imageLink}
              </div>
              <div className="text-base font-medium pt-5 text-black  dark:text-white">
                {item.topic}{" "}
              </div>
              <div className="text-text-default text-base font-normal pt-5 text-gray-500  dark:text-white">
                {item.description}{" "}
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}

export default HippoConnectOffers;
