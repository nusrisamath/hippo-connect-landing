import React from "react";

import Frame from "../../../Images/home/key/Frame.png";

type TKeyFeatures = {
  keyFeatures: {
    id: number;
    topic: string;
    desc: string;
    svg: string;
  }[];
  rightSideImage?: string
}

const KeyFeatures: React.FC<TKeyFeatures> = ({ keyFeatures, rightSideImage = Frame }) => {
  return (
    <div>
      <div className="flex  space-x-9 ">
        <div className="md:w-1/2 flex flex-col space-y-6 ">
          {keyFeatures.map((item) => (
            <div
              className="flex space-x-5 border border-gray-600 border-opacity-20 rounded-lg p-4 md:p-6 bg-gray-100 dark:bg-gray-950"
              key={item.id}
            >
              <div>
                <div className=" flex items-center justify-center rounded-lg w-11 h-11 bg-background-purple-primary">
                  <img src={item.svg} alt={item.topic} />
                </div>
              </div>

              <div className="flex flex-col space-y-2">
                <div className="text-black  dark:text-white text-base font-semibold">
                  {item.topic}
                </div>
                <div className="text-gray-500  dark:text-white text-sm font-normal">
                  {item.desc}
                </div>
              </div>
            </div>
          ))}
        </div>{" "}
        <div className="w-1/2 hidden md:block">
          <img src={rightSideImage} alt="sislphus_logo" className=" w-4/5" />
        </div>
      </div>
    </div>
  );
}

export default KeyFeatures;
