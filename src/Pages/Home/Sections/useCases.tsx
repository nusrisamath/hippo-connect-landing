import React from "react";
import LightCase from "../../../Images/home/usecases/lightcase.svg";
import DarkCase from "../../../Images/home/usecases/darkcase.svg";

const UseCases = () => {
  return (
    <section className="bg-gray-200 dark:bg-gray-900 md:h-screen pt-10" id="usecases">
      <div className="flex justify-center px-5">
        <div className="container">
          <div className="text-neutral-600 text-sm font-normal border-b border-gray-600 border-opacity-20 pb-2">
            USE CASES{" "}
          </div>
          <div className="pb-4 hidden md:block">
            <div className="flex space-x-4 pt-2 mt-5">
              <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
                Connecting
              </div>
              <div className="text-4xl md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                physicians, medical clinics,
              </div>
              <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
                and
              </div>
              <div className="text-4xl md:text-[40px] font-medium bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">
                consumers{" "}
              </div>
            </div>
            <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
              seamlessly – your comprehensive healthcare ecosystem for a{" "}
            </div>
            <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
              healthier future.
            </div>
          </div>
          <div className="pb-4 md:hidden block">
            <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
              Connecting
            </div>
            <div className="bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text  text-[40px] font-medium">
              physicians, medical clinics,
            </div>
            <div className="flex space-x-4">
              <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
                and
              </div>
              <div className="bg-gradient-to-r from-green-500 to-indigo-400 inline-block text-transparent bg-clip-text  text-[40px] font-medium">
                consumers{" "}
              </div>
            </div>
            <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
              seamlessly – your comprehensive healthcare ecosystem for a{" "}
            </div>
            <div className="text-black  dark:text-white text-4xl md:text-[40px] font-medium">
              healthier future.
            </div>
          </div>
          <div>
            <div className="flex justify-center border border-gray-400 rounded-2xl">
              <img src={LightCase} className="w-4/5 dark:hidden" />
              <img src={DarkCase} className="w-4/5 hidden dark:block" />
            </div>

          </div>
        </div>
      </div>
    </section>
  );
}

export default UseCases;
