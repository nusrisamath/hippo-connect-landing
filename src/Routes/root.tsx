import { Outlet } from "react-router-dom";
import Layout from "../Layout";

export default function Root() {
  return (
    <>
      {/* all the other elements */}
      <Layout>
        <Outlet />
      </Layout>
    </>
  );
}
